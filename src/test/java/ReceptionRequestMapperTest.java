import builder.ReceptionRequestBuilder;
import config.MapperConfig;
import dto.ReceptionRequestDto;
import dto.main.MainReceptionRequestDto;
import model.ReceptionRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {MapperConfig.class})
public class ReceptionRequestMapperTest {

    @Autowired
    ModelMapper mapper;

    private ReceptionRequest request =
            ReceptionRequestBuilder.buildReceptionRequest();
    private ReceptionRequestDto requestDto =
            ReceptionRequestBuilder.buildReceptionRequestDto();
    private MainReceptionRequestDto mainRequestDto =
            ReceptionRequestBuilder.buildMainReceptionRequestDto();

    @Test
    void dtoMapperTest() {
        ReceptionRequest result = mapper.map(requestDto, ReceptionRequest.class);

        assertEquals(request.getTimeOfReception(), result.getTimeOfReception());
        assertEquals(request.getReceptionDuration(), result.getReceptionDuration());
        assertEquals(request.getUser(), result.getUser());
    }

    @Test
    void mainDtoMapper() {
        MainReceptionRequestDto result = mapper.map(request, MainReceptionRequestDto.class);

        //ModelMapper can not map enum
        result.setReceptionEnd(result.getReceptionStart()
                .plus(request.getReceptionDuration().getDuration())
        );

        assertEquals(mainRequestDto.getReceptionStart(), result.getReceptionStart());
        assertEquals(mainRequestDto.getReceptionEnd(), result.getReceptionEnd());
        assertEquals(mainRequestDto.getUserFirstName(), result.getUserFirstName());
        assertEquals(mainRequestDto.getUserSecondName(), result.getUserSecondName());
    }

}
