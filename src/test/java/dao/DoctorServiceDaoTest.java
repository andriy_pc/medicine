package dao;

import builder.DoctorBuilder;
import builder.ScheduleBuilder;
import dto.DoctorDto;
import dto.ScheduleDto;
import dto.main.MainDoctorDto;
import dto.main.MainScheduleDto;
import exceptions.NoSuchEntityException;
import model.Doctor;
import model.Schedule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import service.DoctorService;
import service.ScheduleService;
import test.config.TestConfig;

import java.time.DayOfWeek;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class DoctorServiceDaoTest {

    @Autowired
    DoctorService service;

    @Autowired
    ScheduleService scheduleService;

    private Doctor doctor = DoctorBuilder.buildDoctor();
    private DoctorDto doctorDto = DoctorBuilder.buildDoctorDto();
    private MainDoctorDto mainDoctorDto = DoctorBuilder.buildMainDoctorDto();

    private Schedule schedule = ScheduleBuilder.buildSchedule();
    private ScheduleDto scheduleDto = ScheduleBuilder.buildScheduleDto();
    private MainScheduleDto mainScheduleDto = ScheduleBuilder.buildMainScheduleDto();

    @Test
    void findByIdTest() {
        service.save(doctorDto);
        MainDoctorDto result = service.findById(1);
        assertNotNull(result);
    }

    @Test
    public void saveTest() {
        service.save(doctorDto);
        MainDoctorDto result = service.findById(1);
        assertEquals(mainDoctorDto, result);
    }

    @Test
    void updateTestHappy() {
        service.save(doctorDto);
        doctorDto.setFirstName("Changed");
        assertTrue(doctorDto.getFirstName().equals("Changed"));
        service.update(1, doctorDto);
        MainDoctorDto result = service.findById(1);
        MainDoctorDto mainDoctorDtoTest = DoctorBuilder.buildMainDoctorDto();
        mainDoctorDtoTest.setFirstName("Changed");

        assertEquals("Changed", result.getFirstName());
        assertNotEquals(mainDoctorDto, result);
        assertEquals(mainDoctorDtoTest, result);
    }

    @Test
    void updateTestSad() {
        assertThrows(NoSuchEntityException.class, () -> service.update(144, doctorDto));
    }

    @Test
    void deleteTest() {
        service.save(doctorDto);
        assertNotNull(service.findById(1));

        service.delete(1);

        assertThrows(NoSuchEntityException.class, () -> service.findById(1));
    }

    @Test
    void getAllTest() {
        service.save(doctorDto);

        List<MainDoctorDto> result = service.findAll();
        MainDoctorDto findResult = service.findById(1);

        assertNotNull(result);
        assertNotNull(findResult.getSchedules());
        assertNotNull(result.get(0).getSchedules());

        assertEquals(1, result.size());
    }
}
