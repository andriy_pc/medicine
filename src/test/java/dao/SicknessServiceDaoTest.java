package dao;

import builder.PrescriptionBuilder;
import builder.SicknessBuilder;
import builder.UserBuilder;
import dto.PrescriptionDto;
import dto.SicknessDto;
import dto.UserDto;
import dto.main.MainPrescriptionDto;
import dto.main.MainSicknessDto;
import dto.main.MainUserDto;
import exceptions.NoSuchEntityException;
import model.Prescription;
import model.Sickness;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import service.PrescriptionService;
import service.SicknessService;
import service.UserService;
import test.config.TestConfig;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class SicknessServiceDaoTest {

    @Autowired
    SicknessService sicknessService;
    @Autowired
    UserService userService;
    @Autowired
    PrescriptionService prescriptionService;
    @Autowired
    ModelMapper mapper;

    private Sickness sickness = SicknessBuilder.buildSickness();
    private SicknessDto sicknessDto =
            SicknessBuilder.buildSicknessDto();
    private MainSicknessDto mainSicknessDto =
            SicknessBuilder.buildMainSicknessDto();

    private User user = UserBuilder.buildUser();
    private UserDto userDto = UserBuilder.buildUserDto();
    private MainUserDto mainUserDto =
            UserBuilder.buildMainUserDto();

    private Prescription prescription =
            PrescriptionBuilder.buildPrescription();
    private PrescriptionDto prescriptionDto =
            PrescriptionBuilder.buildPrescriptionDto();
    private MainPrescriptionDto mainPrescriptionDto =
            PrescriptionBuilder.buildMainPrescriptionDto();

    @BeforeEach
    void initRequired() {
        userService.save(userDto);
    }

    @Test
    void saveTest() {
        sicknessService.save(sicknessDto);
        MainSicknessDto result = sicknessService.findById(1);
        assertEquals(mainSicknessDto, result);
        assertEquals(mainUserDto, result.getUser());
        assertEquals(mainPrescriptionDto, result.getPrescription());
    }

    @Test
    void findTest() {
        sicknessService.save(sicknessDto);
        MainSicknessDto result = sicknessService.findById(1);

        assertNotNull(result);
        assertEquals(mainSicknessDto, result);
        assertEquals(mainUserDto, result.getUser());
        assertEquals(mainPrescriptionDto, result.getPrescription());
    }

    @Test
    void findTestSad() {
        assertThrows(NoSuchEntityException.class,
                () -> sicknessService.findById(144));
    }

    @Test
    void updateTest() {
        sicknessService.save(sicknessDto);
        sicknessDto.setDiagnosis("Changed!");
        sicknessService.update(1, sicknessDto);

        MainSicknessDto result = sicknessService.findById(1);

        assertNotEquals(mainSicknessDto, result);
    }

    @Test
    void deleteTest() {
        sicknessService.save(sicknessDto);
        sicknessService.delete(1);
        assertThrows(NoSuchEntityException.class, () -> sicknessService.findById(1));
    }

    @Test
    void deleteAllByUserIdTest() {
        sicknessService.save(sicknessDto);
        sicknessService.deleteAllByUserId(1);
        assertThrows(NoSuchEntityException.class,
                () -> sicknessService.findAllByUserId(1));
    }
}
