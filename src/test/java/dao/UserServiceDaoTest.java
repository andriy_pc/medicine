package dao;

import builder.PrescriptionBuilder;
import builder.SicknessBuilder;
import builder.UserBuilder;
import dto.PrescriptionDto;
import dto.SicknessDto;
import dto.UserDto;
import dto.main.MainPrescriptionDto;
import dto.main.MainSicknessDto;
import dto.main.MainUserDto;
import exceptions.NoSuchEntityException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import service.PrescriptionService;
import service.SicknessService;
import service.UserService;
import test.config.TestConfig;

import java.util.List;

import static builder.SicknessBuilder.buildSicknessDto;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
class UserServiceDaoTest {

    @Autowired
    UserService userService;

    @Autowired
    SicknessService sicknessService;

    @Autowired
    PrescriptionService prescriptionService;

    @Test
    void saveTest() {
        UserDto userDto = UserBuilder.buildUserDto();
        MainUserDto mainUserDto = UserBuilder.buildMainUserDto();
        userService.save(userDto);
        MainUserDto foundUser = userService.findById(1);

        assertEquals(mainUserDto, foundUser);
    }

    @Test
    void updateTestHappy() {
        UserDto userDto = UserBuilder.buildUserDto();
        UserDto newUserDto = UserBuilder.buildUserDto();
        MainUserDto newMainUserDto = UserBuilder.buildMainUserDto();
        newUserDto.setFirstName("Updated");
        newMainUserDto.setFirstName("Updated");

        MainUserDto mainUserDto = UserBuilder.buildMainUserDto();
        userService.save(userDto);

        MainUserDto result = userService.update(1, newUserDto);

        assertEquals(newMainUserDto, result);
    }

    @Test
    void updateTestSad() {
        UserDto userDto = UserBuilder.buildUserDto();
        UserDto newUserDto = UserBuilder.buildUserDto();
        MainUserDto newMainUserDto = UserBuilder.buildMainUserDto();
        newUserDto.setFirstName("Updated");
        newMainUserDto.setFirstName("Updated");

        MainUserDto mainUserDto = UserBuilder.buildMainUserDto();
        userService.save(userDto);

        assertThrows(NoSuchEntityException.class, () -> userService.update(144, newUserDto));

    }

    @Test
    void findTest() {
        UserDto userDto = UserBuilder.buildUserDto();
        MainUserDto mainUserDto = UserBuilder.buildMainUserDto();
        userService.save(userDto);

        MainUserDto result = userService.findById(1);

        assertEquals(mainUserDto, result);
    }

    @Test
    void deleteTest() {
        UserDto userDto = UserBuilder.buildUserDto();
        MainUserDto mainUserDto = UserBuilder.buildMainUserDto();
        SicknessDto sicknessDto = SicknessBuilder.buildSicknessDto();
        PrescriptionDto prescriptionDto = PrescriptionBuilder.buildPrescriptionDto();

        sicknessDto.setUser(mainUserDto);
        sicknessDto.setPrescription(prescriptionDto);

        userService.save(userDto);
        MainSicknessDto sick = sicknessService
                .save(sicknessDto);

        userService.delete(1);
        assertThrows(NoSuchEntityException.class,
                () -> userService.findById(1));
        assertThrows(NoSuchEntityException.class,
                () -> sicknessService.findById(1));
        assertThrows(NoSuchEntityException.class,
                () -> prescriptionService.findById(sick.getPrescription().getId()));
    }

    @Test
    void getAllTest() {
        UserDto userDto = UserBuilder.buildUserDto();
        userService.save(userDto);
        userService.save(userDto);

        List<MainUserDto> result = userService.getAll();
        assertNotNull(result);
        assertTrue(result.size() >= 2);
    }

}
