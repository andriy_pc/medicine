package dao;

import builder.DoctorBuilder;
import builder.ReceptionQueueBuilder;
import builder.ReceptionRequestBuilder;
import builder.UserBuilder;
import dto.DoctorDto;
import dto.ReceptionQueueDto;
import dto.ReceptionRequestDto;
import dto.UserDto;
import dto.main.MainDoctorDto;
import dto.main.MainReceptionQueueDto;
import dto.main.MainReceptionRequestDto;
import dto.main.MainUserDto;
import exceptions.NoFreeTimeException;
import exceptions.NoSuchEntityException;
import model.Doctor;
import model.Problem;
import model.ReceptionRequest;
import model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import service.DoctorService;
import service.ReceptionQueueService;
import service.ReceptionRequestService;
import service.UserService;
import test.config.TestConfig;

import java.time.LocalTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class ReceptionRequestServiceDaoTest {

    @Autowired
    private ReceptionRequestService requestService;
    @Autowired
    private ReceptionQueueService queueService;
    @Autowired
    private DoctorService doctorService;
    @Autowired
    private UserService userService;

    @BeforeEach
    void persistRequiredModels() {
        userService.save(userDto);
        doctorService.save(doctorDto);
        queueService.save(queueDto);
    }

    private ReceptionRequest request =
            ReceptionRequestBuilder.buildReceptionRequest();
    private ReceptionRequestDto requestDto =
            ReceptionRequestBuilder.buildReceptionRequestDto();
    private MainReceptionRequestDto mainRequestDto =
            ReceptionRequestBuilder.buildMainReceptionRequestDto();

    private Doctor doctor = DoctorBuilder.buildDoctor();
    private DoctorDto doctorDto = DoctorBuilder.buildDoctorDto();
    private MainDoctorDto mainDoctorDto = DoctorBuilder.buildMainDoctorDto();

    private User user = UserBuilder.buildUser();
    private UserDto userDto = UserBuilder.buildUserDto();
    private MainUserDto mainUserDto = UserBuilder.buildMainUserDto();

    private ReceptionQueueDto queueDto =
            ReceptionQueueBuilder.buildReceptionQueueDto();

    @Test
    void saveTestHappy() {
        MainReceptionQueueDto receptionQueue = queueService.save(queueDto);

        MainReceptionRequestDto result1 = requestService.save(requestDto);
        requestDto.setReceptionDuration(Problem.CHECKUP);
        MainReceptionRequestDto result2 = requestService.save(requestDto);

        assertEquals(mainRequestDto, result1);
        assertNotEquals(mainRequestDto, result2);
    }

    @Test
    void saveTestSad() {
        MainReceptionQueueDto receptionQueue = queueService.save(queueDto);

        MainReceptionRequestDto result1 = requestService.save(requestDto);
        requestDto.setReceptionDuration(Problem.WHOLE_LIFE_CHECK);
        assertThrows(NoFreeTimeException.class,
                () -> requestService.save(requestDto));
    }

    @Test
    void findByIdTest() {
        requestService.save(requestDto);
        MainReceptionRequestDto result = requestService.findById(1);

        assertEquals(mainRequestDto, result);
    }

    @Test
    void findByQueueIdTest() {
        requestService.save(requestDto);
        requestDto.setReceptionDuration(Problem.CONSULTATION);
        requestService.save(requestDto);

        List<MainReceptionRequestDto> result = requestService.findByQueueId(1);

        assertNotNull(result);
        assertEquals(2, result.size());
        assertEquals(mainRequestDto, result.get(0));
        assertNotEquals(mainRequestDto, result.get(1));
    }

    @Test
    void updateTestHappy() {
        requestService.save(requestDto);
        MainReceptionRequestDto beforeUpdate = requestService.findById(1);

        requestDto.setReceptionDuration(Problem.CHECKUP);
        requestService.update(1, requestDto);
        MainReceptionRequestDto afterUpdate = requestService.findById(1);
        mainRequestDto.setReceptionEnd(LocalTime.of(15, 25));
        mainRequestDto.setProblem(Problem.CHECKUP);

        assertNotEquals(beforeUpdate, afterUpdate);
        assertEquals(mainRequestDto, afterUpdate);
    }

    @Test
    void updateTestSad() {
        requestService.save(requestDto);

        requestDto.setReceptionDuration(Problem.WHOLE_LIFE_CHECK);
        assertThrows(NoFreeTimeException.class, () -> requestService.update(1, requestDto));
    }

    @Test
    void deleteTest() {
        requestService.save(requestDto);
        MainReceptionRequestDto requestDto = requestService.findById(1);

        requestService.delete(1);

        assertNotNull(requestDto);
        assertThrows(NoSuchEntityException.class, () -> requestService.findById(1));
    }
}
