package dao;

import builder.PrescriptionBuilder;
import dto.PrescriptionDto;
import dto.main.MainPrescriptionDto;
import exceptions.NoSuchEntityException;
import model.Prescription;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import service.PrescriptionService;
import test.config.TestConfig;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)

class PrescriptionServiceDaoTest {

    @Autowired
    PrescriptionService prescriptionService;

    @Autowired
    ModelMapper mapper;

    private Prescription prescription =
            PrescriptionBuilder.buildPrescription();
    private PrescriptionDto prescriptionDto =
            PrescriptionBuilder.buildPrescriptionDto();
    private MainPrescriptionDto mainPrescriptionDto =
            PrescriptionBuilder.buildMainPrescriptionDto();

    @Test
    void saveTest() {
        MainPrescriptionDto saveResult = prescriptionService.save(prescriptionDto);
        MainPrescriptionDto findResult = prescriptionService.findById(1);

        assertEquals(mainPrescriptionDto, saveResult);
        assertEquals(mainPrescriptionDto, findResult);

    }

 @Test
    void findByIdTest() {
        MainPrescriptionDto saveResult = prescriptionService.save(prescriptionDto);
        MainPrescriptionDto findResult = prescriptionService.findById(1);

        assertNotNull(findResult);
        assertEquals(mainPrescriptionDto, saveResult);
    }


    @Test
    void updateTestHappy() {
        MainPrescriptionDto saveResult = prescriptionService.save(prescriptionDto);

        prescriptionDto.setDose("Dose was changed!");
        mainPrescriptionDto.setDose("Dose was changed!");
        MainPrescriptionDto updateResult = prescriptionService.update(1, prescriptionDto);
        assertEquals(mainPrescriptionDto, updateResult);

        MainPrescriptionDto result = prescriptionService.findById(1);
        assertEquals(mainPrescriptionDto, result);
    }

    @Test
    void updateSad() {
        assertThrows(NoSuchEntityException.class,
                () -> prescriptionService.update(144, prescriptionDto));
    }

    @Test
    void removeTest() {
        MainPrescriptionDto saveResult = prescriptionService.save(prescriptionDto);

        MainPrescriptionDto findResult = prescriptionService.findById(1);

        prescriptionService.delete(findResult.getId());
        assertThrows(NoSuchEntityException.class, () -> prescriptionService.findById(findResult.getId()));
    }
}
