package dao;

import builder.ScheduleBuilder;
import dto.ScheduleDto;
import dto.main.MainScheduleDto;
import exceptions.NoSuchEntityException;
import model.Day;
import model.Schedule;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import service.ScheduleService;
import test.config.TestConfig;

import java.time.DayOfWeek;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class ScheduleServiceDaoTest {

    @Autowired
    private ScheduleService scheduleService;

    private Schedule schedule = ScheduleBuilder.buildSchedule();
    private ScheduleDto scheduleDto = ScheduleBuilder.buildScheduleDto();
    private MainScheduleDto mainScheduleDto = ScheduleBuilder.buildMainScheduleDto();

    @Test
    void saveTest() {
        MainScheduleDto saveResult = scheduleService.save(scheduleDto);
        MainScheduleDto result = scheduleService.findById(1);

        assertEquals(mainScheduleDto.getId(), saveResult.getId());
        assertEquals(mainScheduleDto.getDay(), saveResult.getDay());
        assertEquals(mainScheduleDto.getId(), result.getId());
        assertEquals(mainScheduleDto.getDay(), result.getDay());
    }

    @Test
    void findByIdTest() {
        MainScheduleDto saveResult = scheduleService.save(scheduleDto);
        MainScheduleDto result = scheduleService.findById(1);

        assertEquals(mainScheduleDto.getId(), result.getId());
    }

    @Test
    void updateTest() {
        scheduleService.save(scheduleDto);
        MainScheduleDto result = scheduleService.findById(1);

        assertEquals(mainScheduleDto.getId(), result.getId());
        assertEquals(mainScheduleDto.getDay(), result.getDay());

        scheduleDto.setDay(DayOfWeek.SUNDAY);
        scheduleService.update(1, scheduleDto);
        result = scheduleService.findById(1);
        assertNotEquals(mainScheduleDto, result);
    }

    @Test
    void removeTest() {
        scheduleService.save(scheduleDto);
        MainScheduleDto result = scheduleService.findById(1);

        assertEquals(mainScheduleDto.getId(), result.getId());
        assertEquals(mainScheduleDto.getDay(), result.getDay());

        scheduleService.delete(1);

        assertThrows(NoSuchEntityException.class, () -> scheduleService.findById(1));
    }

}
