package dao;

import builder.DoctorBuilder;
import builder.ReceptionQueueBuilder;
import dto.DoctorDto;
import dto.ReceptionQueueDto;
import dto.main.MainDoctorDto;
import dto.main.MainReceptionQueueDto;
import exceptions.NoSuchEntityException;
import model.Doctor;
import model.ReceptionQueue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import service.DoctorService;
import service.ReceptionQueueService;
import test.config.TestConfig;

import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = {TestConfig.class})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class ReceptionQueueServiceDaoTest {

    @Autowired
    private ReceptionQueueService queueService;
    @Autowired
    private DoctorService doctorService;

    private Doctor doctor = DoctorBuilder.buildDoctor();
    private DoctorDto doctorDto =
            DoctorBuilder.buildDoctorDto();
    private MainDoctorDto mainDoctorDto =
            DoctorBuilder.buildMainDoctorDto();

    private ReceptionQueue queue =
            ReceptionQueueBuilder.buildReceptionQueue();
    private ReceptionQueueDto queueDto =
            ReceptionQueueBuilder.buildReceptionQueueDto();
    private MainReceptionQueueDto mainQueueDto =
            ReceptionQueueBuilder.buildMainReceptionQueueDto();


    @BeforeEach
    void persistRequired() {
        doctorService.save(doctorDto);
    }

    @Test
    void saveTest() {
        MainReceptionQueueDto result = queueService.save(queueDto);

        assertNotNull(result);
        assertEquals(mainQueueDto, result);
        assertTrue(result.getRequests().isEmpty());
    }

    @Test
    void findByIdTestHappy() {
        queueService.save(queueDto);
        MainReceptionQueueDto result = queueService.findById(1);

        assertNotNull(result);
        assertEquals(mainQueueDto, result);
        assertTrue(result.getRequests().isEmpty());
    }

    @Test
    void findByIdTestSad() {
        assertThrows(NoSuchEntityException.class,
                () -> queueService.findById(144));
    }

    @Test
    void findByDateForDoctorTest() {
        queueService.save(queueDto);

        List<MainReceptionQueueDto> result =
                queueService.findAllByDateForDoctor(LocalDate.of(2020, 1, 1), 1);

        assertTrue(result.size() == 1);
        MainReceptionQueueDto resultQueue = result.get(0);
        assertEquals(mainQueueDto, resultQueue);
    }

    @Test
    void updateTestSad() {
        queueService.save(queueDto);
        MainReceptionQueueDto beforeUpdate = queueService.findById(1);

        queueDto.setDate(LocalDate.now());
        mainQueueDto.setDate(LocalDate.now());

        assertThrows(NoSuchEntityException.class,
                () -> queueService.update(1, queueDto));
        
    }

    @Test
    void updateTestHappy() {
        queueService.save(queueDto);
        MainReceptionQueueDto beforeUpdate = queueService.findById(1);

        queueDto.setDate(LocalDate.of(2020, 1, 8));
        mainQueueDto.setDate(LocalDate.of(2020, 1, 8));

        queueService.update(1, queueDto);

        MainReceptionQueueDto afterUpdate = queueService.findById(1);
        assertNotEquals(beforeUpdate, afterUpdate);
        assertNotNull(afterUpdate);
        assertEquals(mainQueueDto, afterUpdate);
        assertTrue(beforeUpdate.getRequests().isEmpty());
        assertTrue(afterUpdate.getRequests().isEmpty());
    }

    @Test
    void deleteTest() {
        queueService.save(queueDto);
        MainReceptionQueueDto findResult = queueService.findById(1);
        assertNotNull(findResult);
        queueService.delete(1);

        assertThrows(NoSuchEntityException.class, () -> queueService.findById(1));
    }
}
