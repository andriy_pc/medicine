package service;

import builder.PrescriptionBuilder;
import dao.PrescriptionDao;
import dto.PrescriptionDto;
import dto.main.MainPrescriptionDto;
import model.Prescription;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PrescriptionServiceTest {

    @Mock
    PrescriptionDao dao;

    @Mock
    ModelMapper mapper;

    @InjectMocks
    PrescriptionService service;

    private Prescription prescription =
            PrescriptionBuilder.buildPrescription();
    private PrescriptionDto prescriptionDto =
            PrescriptionBuilder.buildPrescriptionDto();
    private MainPrescriptionDto mainPrescriptionDto =
            PrescriptionBuilder.buildMainPrescriptionDto();


    @BeforeAll
    void init() {
        MockitoAnnotations.initMocks(this);

        when(mapper.map(Mockito.any(PrescriptionDto.class),
                Mockito.eq(Prescription.class))).thenReturn(prescription);
        when(mapper.map(Mockito.any(Prescription.class),
                Mockito.eq(MainPrescriptionDto.class))).thenReturn(mainPrescriptionDto);

        when(dao.save(Mockito.any(Prescription.class))).thenReturn(prescription);
        when(dao.update(Mockito.any(Prescription.class))).thenReturn(prescription);
        when(dao.findById(Mockito.any(long.class))).thenReturn(prescription);
    }

    @Test
    void saveTest() {
        MainPrescriptionDto result = service.save(prescriptionDto);
        assertEquals(mainPrescriptionDto, result);
    }

    @Test
    void findByIdTest() {
        MainPrescriptionDto result = service.findById(1);
        assertEquals(mainPrescriptionDto, result);
    }

    @Test
    void updateTest() {
        MainPrescriptionDto result = service.update(1, prescriptionDto);
        assertEquals(mainPrescriptionDto, result);
    }
}
