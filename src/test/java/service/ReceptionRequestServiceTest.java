package service;

import builder.ReceptionRequestBuilder;
import dao.ReceptionRequestDao;
import dto.ReceptionRequestDto;
import dto.main.MainReceptionRequestDto;
import model.ReceptionRequest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.time.LocalTime;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReceptionRequestServiceTest {


    @Mock
    private ReceptionRequestDao dao;

    @Mock
    private ModelMapper mapper;

    @InjectMocks
    ReceptionRequestService service;

    private ReceptionRequest request =
            ReceptionRequestBuilder.buildReceptionRequest();
    private ReceptionRequestDto requestDto =
            ReceptionRequestBuilder.buildReceptionRequestDto();
    private MainReceptionRequestDto mainRequestDto =
            ReceptionRequestBuilder.buildMainReceptionRequestDto();

    @BeforeAll
    void init() {
        MockitoAnnotations.initMocks(this);

        Mockito.when(mapper.map(Mockito.any(ReceptionRequestDto.class),
                Mockito.eq(ReceptionRequest.class)))
                .thenReturn(request);
        Mockito.when(mapper.map(Mockito.any(ReceptionRequest.class),
                Mockito.eq(MainReceptionRequestDto.class)))
                .thenReturn(mainRequestDto);

        Mockito.when(dao.save(Mockito.any(ReceptionRequest.class))).thenReturn(request);
        Mockito.when(dao.findById(Mockito.any(long.class))).thenReturn(request);
        Mockito.when(dao.update(Mockito.any(ReceptionRequest.class))).thenReturn(request);
    }

    @Test
    void saveTest() {
        MainReceptionRequestDto result = service.save(requestDto);

        assertEquals(mainRequestDto, result);
    }

    @Test
    void findTest() {
        MainReceptionRequestDto result = service.findById(1);

        assertEquals(mainRequestDto, result);
    }

    @Test
    void updateTest() {
        MainReceptionRequestDto result = service.update(1, requestDto);

        assertEquals(mainRequestDto, result);
    }

    @Test
    void calculateReceptionEndTest() {
        MainReceptionRequestDto result = service.findById(1);
        assertNotNull(result.getReceptionEnd());
        assertEquals(LocalTime.of(15, 15), result.getReceptionEnd());
    }


    @Test
    void calculateReceptionEndTestSadBuTrue() {
        MainReceptionRequestDto result = service.findById(1);

        mainRequestDto.setReceptionEnd(LocalTime.of(19, 25));

        assertNotNull(result.getReceptionEnd());
        assertNotEquals(LocalTime.of(15, 15), result.getReceptionEnd());
    }
}
