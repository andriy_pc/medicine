package service;

import builder.DoctorBuilder;
import dao.DoctorDao;
import dto.DoctorDto;
import dto.main.MainDoctorDto;
import model.Doctor;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class DoctorServiceTest {

    @InjectMocks
    DoctorService service;

    @Mock
    DoctorDao dao;

    @Mock
    ModelMapper mapper;

    private Doctor doctor = DoctorBuilder.buildDoctor();
    private DoctorDto doctorDto = DoctorBuilder.buildDoctorDto();
    private MainDoctorDto mainDoctorDto = DoctorBuilder.buildMainDoctorDto();

    @BeforeAll
    void init() {
        MockitoAnnotations.initMocks(this);

        when(mapper.map(Mockito.any(DoctorDto.class), Mockito.eq(Doctor.class))).thenReturn(doctor);
        when(mapper.map(Mockito.any(Doctor.class), Mockito.eq(MainDoctorDto.class))).thenReturn(mainDoctorDto);

        when(dao.save(Mockito.any(Doctor.class))).thenReturn(doctor);
        when(dao.findById(Mockito.any(long.class))).thenReturn(doctor);
        when(dao.update(Mockito.any(Doctor.class))).thenReturn(doctor);
        when(dao.getAll()).thenReturn(Arrays.asList(doctor, doctor, doctor));
    }

    @Test
    void saveTest() {
        MainDoctorDto result = service.save(doctorDto);
        assertEquals(mainDoctorDto, result);
    }

    @Test
    void findTest() {
        MainDoctorDto result = service.findById(1);
        assertEquals(mainDoctorDto, result);
    }

    @Test
    void updateTest() {
        MainDoctorDto result = service.update(1, doctorDto);
        assertEquals(mainDoctorDto, result);
    }

    @Test
    void getAllTest() {
        assertEquals(3, service.findAll().size());
    }
}
