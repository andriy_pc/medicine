package service;

import builder.SicknessBuilder;
import dao.SicknessDao;
import dto.DoctorDto;
import dto.SicknessDto;
import dto.main.MainDoctorDto;
import dto.main.MainSicknessDto;
import model.Doctor;
import model.Sickness;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SicknessServiceTest {

    @Mock
    SicknessDao dao;

    @Mock
    ModelMapper mapper;

    @InjectMocks
    SicknessService service;

    private Sickness emptySickness = SicknessBuilder.buildEmptySickness();
    private SicknessDto emptySicknessDto = SicknessBuilder.buildEmptySicknessDto();
    private MainSicknessDto mainEmptySicknessDto = SicknessBuilder.buildEmptyMainSicknessDto();

    @BeforeAll
    void init() {
        MockitoAnnotations.initMocks(this);
        when(mapper.map(Mockito.any(SicknessDto.class), Mockito.eq(Sickness.class))).thenReturn(emptySickness);
        when(mapper.map(Mockito.any(Sickness.class), Mockito.eq(MainSicknessDto.class))).thenReturn(mainEmptySicknessDto);

        when(dao.save(Mockito.any(Sickness.class))).thenReturn(emptySickness);
        when(dao.findById(Mockito.any(long.class))).thenReturn(emptySickness);
        when(dao.update(Mockito.any(Sickness.class))).thenReturn(emptySickness);
        when(dao.findAllByUserId(Mockito.any(long.class))).thenReturn(Arrays.asList(emptySickness, emptySickness, emptySickness));

    }

    @Test
    void saveTest() {
        MainSicknessDto result = service.save(emptySicknessDto);

        assertNotNull(result);
        assertEquals(mainEmptySicknessDto, result);
    }

    @Test
    void findByIdTest() {
        MainSicknessDto result = service.findById(1);

        assertNotNull(result);
        assertEquals(mainEmptySicknessDto, result);
    }

    @Test
    void updateTest() {
        emptySicknessDto.setDiagnosis("Something wrong!");
        MainSicknessDto result = service.update(1, emptySicknessDto);

        assertEquals(mainEmptySicknessDto, result);
    }

    @Test
    void findAllByUserIdTest() {
        List<MainSicknessDto> result = service.findAllByUserId(1);

        assertNotNull(result);
        assertEquals(3, result.size());
    }
}
