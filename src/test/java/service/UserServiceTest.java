package service;

import builder.UserBuilder;
import dao.UserDao;
import dto.UserDto;
import dto.main.MainUserDto;
import exceptions.NoSuchEntityException;
import model.User;
import org.junit.jupiter.api.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceTest {

    @BeforeAll
    public void initMocks() {
        MockitoAnnotations.initMocks(this);

        when(mapper.map(Mockito.any(UserDto.class), Mockito.eq(User.class))).thenReturn(user);
        when(mapper.map(Mockito.any(User.class), Mockito.eq(MainUserDto.class))).thenReturn(mainUserDto);
        when(userDao.save(Mockito.any(User.class))).thenReturn(user);
        when(userDao.find(Mockito.any(long.class))).thenReturn(user);
        when(userDao.update(Mockito.any(User.class))).thenReturn(user);

    }


    @Mock
    static UserDao userDao;

    @Mock
    static ModelMapper mapper;

    @InjectMocks
    static UserService userService;

    private User user = UserBuilder.buildUser();
    private UserDto userDto = UserBuilder.buildUserDto();
    private MainUserDto mainUserDto = UserBuilder.buildMainUserDto();

    @Test
    void saveUserTest() {

        MainUserDto savingResult = userService.save(userDto);

        Assertions.assertEquals(user.getId(), savingResult.getId());
        Assertions.assertEquals(user.getFirstName(), savingResult.getFirstName());
        Assertions.assertEquals(user.getSecondName(), savingResult.getSecondName());
        Assertions.assertEquals(user.getDateOfBirth().toString(), savingResult.getDateOfBirth());
        Assertions.assertEquals(user.getGender(), savingResult.getGender());
        Assertions.assertEquals(user.getPhoneNumber(), savingResult.getPhoneNumber());
        Assertions.assertEquals(user.getEmail(), savingResult.getEmail());
        Assertions.assertEquals(user.getDateOfRegistration().toString(), savingResult.getDateOfRegistration());
    }

    @Test
    void updateUserTestHappy() {
        User user = UserBuilder.buildUser();
        UserDto userDto = UserBuilder.buildUserDto();
        MainUserDto mainUserDto = UserBuilder.buildMainUserDto();

        MainUserDto updateResult = userService.update(1, userDto);

        Assertions.assertEquals(userDto.getFirstName(), updateResult.getFirstName());
        Assertions.assertEquals(userDto.getSecondName(), updateResult.getSecondName());
        Assertions.assertEquals(userDto.getDateOfBirth(), updateResult.getDateOfBirth());
        Assertions.assertEquals(userDto.getGender(), updateResult.getGender());
        Assertions.assertEquals(userDto.getPhoneNumber(), updateResult.getPhoneNumber());
        Assertions.assertEquals(userDto.getEmail(), updateResult.getEmail());
    }

    @Test
    void updateTestSad() {
        User user = UserBuilder.buildUser();
        UserDto userDto = UserBuilder.buildUserDto();
        MainUserDto mainUserDto = UserBuilder.buildMainUserDto();

        when(userDao.find(Mockito.any(long.class))).thenReturn(null);

        assertThrows(NoSuchEntityException.class, () -> userService.update(1, userDto));
    }

    @Test
    void findUserTest() {
        User user = UserBuilder.buildUser();
        UserDto userDto = UserBuilder.buildUserDto();
        MainUserDto mainUserDto = UserBuilder.buildMainUserDto();

        MainUserDto findResult = userService.findById(1);

        Assertions.assertEquals(user.getId(), findResult.getId());
        Assertions.assertEquals(user.getFirstName(), findResult.getFirstName());
        Assertions.assertEquals(user.getSecondName(), findResult.getSecondName());
        Assertions.assertEquals(user.getDateOfBirth().toString(), findResult.getDateOfBirth());
        Assertions.assertEquals(user.getGender(), findResult.getGender());
        Assertions.assertEquals(user.getPhoneNumber(), findResult.getPhoneNumber());
        Assertions.assertEquals(user.getEmail(), findResult.getEmail());
        Assertions.assertEquals(user.getDateOfRegistration().toString(), findResult.getDateOfRegistration());
    }
}
