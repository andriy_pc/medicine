package service;

import builder.ScheduleBuilder;
import dao.ScheduleDao;
import dto.ScheduleDto;
import dto.main.MainScheduleDto;
import model.Schedule;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ScheduleServiceTest {

    @BeforeAll
    void init() {
        MockitoAnnotations.initMocks(this);
        when(mapper.map(Mockito.any(ScheduleDto.class), Mockito.eq(Schedule.class))).thenReturn(schedule);
        when(mapper.map(Mockito.any(Schedule.class), Mockito.eq(MainScheduleDto.class))).thenReturn(mainScheduleDto);

        when(dao.save(Mockito.any(Schedule.class))).thenReturn(schedule);
        when(dao.update(Mockito.any(Schedule.class))).thenReturn(schedule);
        when(dao.findById(Mockito.any(long.class))).thenReturn(schedule);
    }

    @Mock
    ScheduleDao dao;

    @Mock
    ModelMapper mapper;

    @InjectMocks
    ScheduleService service;

    private Schedule schedule = ScheduleBuilder.buildSchedule();
    private ScheduleDto scheduleDto = ScheduleBuilder.buildScheduleDto();
    private MainScheduleDto mainScheduleDto = ScheduleBuilder.buildMainScheduleDto();

    @Test
    void saveTest() {
        MainScheduleDto result = service.save(scheduleDto);

        assertEquals(mainScheduleDto, result);
    }

    @Test
    void findTest() {
        MainScheduleDto result = service.findById(1);
        assertNotNull(result);
    }

    @Test
    void updateTest() {
        MainScheduleDto result = service.update(1, scheduleDto);
        assertEquals(mainScheduleDto, result);
    }
}
