package builder;

import dto.ScheduleDto;
import dto.main.MainScheduleDto;
import model.Day;
import model.Schedule;

import java.time.DayOfWeek;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class ScheduleBuilder {
    public static Schedule buildSchedule() {
        return Schedule.builder()
                .id(1)
                .day(DayOfWeek.WEDNESDAY)
                .startTime(LocalTime.of(8, 0))
                .endTime(LocalTime.of(20, 0))
                .breakStartTime(LocalTime.now())
                .breakEndTime(LocalTime.now())
                .build();
    }

    public static Schedule buildSchedule(long id) {
        return Schedule.builder()
                .id(id)
                .day(DayOfWeek.WEDNESDAY)
                .startTime(LocalTime.of(8, 0))
                .endTime(LocalTime.of(20, 0))
                .breakStartTime(LocalTime.now())
                .breakEndTime(LocalTime.now())
                .build();
    }

    public static ScheduleDto buildScheduleDto() {
        return ScheduleDto.builder()
                .day(DayOfWeek.WEDNESDAY)
                .startTime("08:00")
                .endTime("20:00")
                .breakStartTime("13:00")
                .breakEndTime("14:00")
                .build();
    }

    public static MainScheduleDto buildMainScheduleDto() {
        return MainScheduleDto.builder()
                .id(1)
                .day(DayOfWeek.WEDNESDAY)
                .startTime("08:00")
                .endTime("20:00")
                .breakStartTime("13:00")
                .breakEndTime("14:00")
                .build();
    }

    public static MainScheduleDto buildMainScheduleDto(long id) {
        return MainScheduleDto.builder()
                .id(id)
                .day(DayOfWeek.WEDNESDAY)
                .startTime("08:00")
                .endTime("20:00")
                .breakStartTime("13:00")
                .breakEndTime("14:00")
                .build();
    }

    public static Set<Schedule> getSchedules() {
        return new TreeSet<>(Arrays.asList(
                buildSchedule(1),
                buildSchedule(2),
                buildSchedule(3))
        );
    }

    public static Set<ScheduleDto> getSchedulesDto() {
        return new TreeSet<>(Arrays.asList(
                buildScheduleDto(),
                buildScheduleDto(),
                buildScheduleDto())
        );
    }

    public static Set<MainScheduleDto> getMainSchedules() {
        return new TreeSet<>(Arrays.asList(
                buildMainScheduleDto(1),
                buildMainScheduleDto(2),
                buildMainScheduleDto(3)
        ));
    }
}
