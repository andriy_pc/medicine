package builder;

import dto.PrescriptionDto;
import dto.main.MainPrescriptionDto;
import model.Prescription;

public class PrescriptionBuilder {
    public static Prescription buildPrescription() {
        return Prescription.builder()
                .id(1)
                .preparation("Dufalak")
                .applicationMethod("oral method")
                .dose("a few")
                .frequency("very much!")
                .build();
    }

    public static PrescriptionDto buildPrescriptionDto() {
        return PrescriptionDto.builder()
                .preparation("Dufalak")
                .applicationMethod("oral method")
                .dose("a few")
                .frequency("very much!")
                .build();
    }

    public static MainPrescriptionDto buildMainPrescriptionDto() {
        return MainPrescriptionDto.builder()
                .id(1)
                .preparation("Dufalak")
                .applicationMethod("oral method")
                .dose("a few")
                .frequency("very much!")
                .build();
    }
}
