package builder;

import dto.ReceptionQueueDto;
import dto.main.MainReceptionQueueDto;
import model.Day;
import model.ReceptionQueue;
import model.ReceptionRequest;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class ReceptionQueueBuilder {
    public static ReceptionQueue buildReceptionQueue() {
        return ReceptionQueue.builder()
                .id(1)
                .date(LocalDate.of(2020, 1, 1))
                .start(LocalTime.of(8, 0))
                .end(LocalTime.of(20, 0))
                .freeTime(Duration.ofHours(12))
                .day(DayOfWeek.WEDNESDAY)
                .doctor(DoctorBuilder.buildDoctor())
                .build();
    }

    public static ReceptionQueueDto buildReceptionQueueDto() {
        return ReceptionQueueDto.builder()
                .date(LocalDate.of(2020, 1, 1))
                .doctor(DoctorBuilder.buildMainDoctorDto())
                .build();
    }

    public static MainReceptionQueueDto buildMainReceptionQueueDto() {
        return MainReceptionQueueDto.builder()
                .id(1)
                .date(LocalDate.of(2020, 1, 1))
                .doctor(DoctorBuilder.buildMainDoctorDto())
                .day(DayOfWeek.WEDNESDAY)
                .start(LocalTime.of(8, 0))
                .end(LocalTime.of(20, 0))
                .freeTime(Duration.ofHours(12))
                .requests(new ArrayList<>())
                .build();
    }
}
