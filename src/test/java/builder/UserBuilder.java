package builder;

import dto.UserDto;
import dto.main.MainUserDto;
import model.Gender;
import model.User;

import java.time.LocalDate;

public class UserBuilder {

    public static User buildUser() {
        return User.builder()
                .id(1)
                .firstName("Ruslan")
                .secondName("Holovatsky")
                .dateOfBirth(LocalDate.of(2020, 1, 1))
                .gender(Gender.DECEPTICON)
                .phoneNumber("+3802200000")
                .email("ruslan@da.or")
                .dateOfRegistration(LocalDate.now())
                .build();
    }

    public static UserDto buildUserDto() {
        return UserDto.builder()
                .firstName("Ruslan")
                .secondName("Holovatsky")
                .dateOfBirth("2020-01-01")
                .gender(Gender.DECEPTICON)
                .phoneNumber("+3802200000")
                .email("ruslan@da.or")
                .build();
    }

    public static MainUserDto buildMainUserDto() {
        return MainUserDto.builder()
                .id(1)
                .firstName("Ruslan")
                .secondName("Holovatsky")
                .dateOfBirth("2020-01-01")
                .gender(Gender.DECEPTICON)
                .phoneNumber("+3802200000")
                .email("ruslan@da.or")
                .dateOfRegistration(LocalDate.now().toString())
                .build();
    }
}
