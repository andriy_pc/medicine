package builder;

import dto.SicknessDto;
import dto.main.MainSicknessDto;
import model.Sickness;

import java.time.LocalDate;

public class SicknessBuilder {

    //empty means no user and prescription
    public static Sickness buildEmptySickness() {
        return Sickness.builder()
                .id(1)
                .user(null)
                .description("short description")
                .diagnosis("I wish i know...")
                .prescription(null)
                .dateOfBeginning(LocalDate.now())
                .dateOfEnding(LocalDate.now())
                .build();
    }
    public static SicknessDto buildEmptySicknessDto() {
        return SicknessDto.builder()
                .user(null)
                .description("short description")
                .diagnosis("I wish i know...")
                .prescription(null)
                .dateOfBeginning(LocalDate.now())
                .dateOfEnding(LocalDate.now())
                .build();
    }
    public static MainSicknessDto buildEmptyMainSicknessDto() {
        return MainSicknessDto.builder()
                .id(1)
                .user(null)
                .description("short description")
                .diagnosis("I wish i know...")
                .prescription(null)
                .dateOfBeginning(LocalDate.now())
                .dateOfEnding(LocalDate.now())
                .build();
    }

    public static Sickness buildSickness() {
        return Sickness.builder()
                .id(1)
                .user(UserBuilder.buildUser())
                .description("short description")
                .diagnosis("I wish i know...")
                .prescription(PrescriptionBuilder.buildPrescription())
                .dateOfBeginning(LocalDate.now())
                .dateOfEnding(LocalDate.now())
                .build();
    }

    public static SicknessDto buildSicknessDto() {
        return SicknessDto.builder()
                .user(UserBuilder.buildMainUserDto())
                .description("short description")
                .diagnosis("I wish i know...")
                .prescription(PrescriptionBuilder.buildPrescriptionDto())
                .dateOfBeginning(LocalDate.now())
                .dateOfEnding(LocalDate.now())
                .build();
    }

    public static MainSicknessDto buildMainSicknessDto() {
        return MainSicknessDto.builder()
                .id(1)
                .user(UserBuilder.buildMainUserDto())
                .description("short description")
                .diagnosis("I wish i know...")
                .prescription(PrescriptionBuilder.buildMainPrescriptionDto())
                .dateOfBeginning(LocalDate.now())
                .dateOfEnding(LocalDate.now())
                .build();
    }
}
