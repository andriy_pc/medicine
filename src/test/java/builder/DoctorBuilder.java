package builder;

import dto.DoctorDto;
import dto.main.MainDoctorDto;
import model.Doctor;
import model.Gender;
import model.Schedule;

import java.time.LocalDate;

public class DoctorBuilder {

    public static Doctor buildDoctor() {
        return Doctor.builder()
                .id(1)
                .firstName("Doctor")
                .secondName("Zlo")
                .dateOfBirth(LocalDate.now())
                .gender(Gender.MALE)
                .phoneNumber("+666666666")
                .email("ruslan@da.or")
                .specialization("pediatrician")
                .schedules(ScheduleBuilder.getSchedules())
                .build();
    }

    public static DoctorDto buildDoctorDto() {
        return DoctorDto.builder()
                .firstName("Doctor")
                .secondName("Zlo")
                .dateOfBirth("2020-01-01")
                .gender(Gender.MALE)
                .phoneNumber("+666666666")
                .email("ruslan@da.or")
                .specialization("pediatrician")
                .schedules(ScheduleBuilder.getSchedulesDto())
                .build();
    }

    public static MainDoctorDto buildMainDoctorDto() {
        return MainDoctorDto.builder()
                .id(1)
                .firstName("Doctor")
                .secondName("Zlo")
                .dateOfBirth("2020-01-01")
                .gender(Gender.MALE)
                .phoneNumber("+666666666")
                .email("ruslan@da.or")
                .specialization("pediatrician")
                .schedules(ScheduleBuilder.getMainSchedules())
                .build();
    }
}
