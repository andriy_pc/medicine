package builder;

import dto.ReceptionRequestDto;
import dto.main.MainReceptionRequestDto;
import model.Problem;
import model.ReceptionRequest;

import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;

public class ReceptionRequestBuilder {
    public static ReceptionRequest buildReceptionRequest() {
        return ReceptionRequest.builder()
                .id(1)
                .timeOfReception(LocalTime.of(15, 0))
                .receptionDuration(Problem.MEDICAL_EXAMINATION)
                .shortDescription("A little about disease")
                .user(UserBuilder.buildUser())
                .queue(ReceptionQueueBuilder.buildReceptionQueue())
                .build();
    }

    public static ReceptionRequestDto buildReceptionRequestDto() {
        return ReceptionRequestDto.builder()
                .timeOfReception(LocalTime.of(15, 0))
                .receptionDuration(Problem.MEDICAL_EXAMINATION)
                .shortDescription("A little about disease")
                .user(UserBuilder.buildUser())
                .queue(ReceptionQueueBuilder.buildMainReceptionQueueDto())
                .build();
    }

    public static MainReceptionRequestDto buildMainReceptionRequestDto() {
        return MainReceptionRequestDto.builder()
                .id(1)
                .receptionStart(LocalTime.of(15, 0))
                .receptionEnd(LocalTime.of(15, 15))
                .problem(Problem.MEDICAL_EXAMINATION)
                .userFirstName("Ruslan")
                .userSecondName("Holovatsky")
                .queue(ReceptionQueueBuilder.buildMainReceptionQueueDto())
                .build();
    }

    public static List<MainReceptionRequestDto> getListOfRequests() {
        return Arrays.asList(
                buildMainReceptionRequestDto(),
                buildMainReceptionRequestDto(),
                buildMainReceptionRequestDto()
        );
    }
}
