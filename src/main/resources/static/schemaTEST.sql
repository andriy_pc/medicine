CREATE TABLE IF NOT EXISTS users
(
     id SERIAL PRIMARY KEY,
     first_name VARCHAR(50) NOT NULL,
     second_name VARCHAR(50) NOT NULL,
     date_of_birth DATE NOT NULL,
     gender VARCHAR(10) NOT NULL,
     phone_number VARCHAR(11) NOT NULL,
     email VARCHAR(50) NOT NULL,
     date_of_registration DATE NOT NULL
);

CREATE TABLE IF NOT EXISTS doctor
(
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    second_name VARCHAR(50) NOT NULL,
    date_of_birth DATE NOT NULL,
    gender VARCHAR(10) NOT NULL,
    phone_number VARCHAR(10) NOT NULL,
    email VARCHAR(25) NOT NULL,
    specialization VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS schedule
(
    id SERIAL PRIMARY KEY,
    day VARCHAR(10) NOT NULL,
    start_time TIME,
    end_time TIME,
    break_start_time TIME,
    break_end_time TIME,
    doctor_id INT,

    CONSTRAINT fk_doctor_id
        FOREIGN KEY (doctor_id)
            REFERENCES doctor (id)
);

CREATE TABLE IF NOT EXISTS prescription
(
    id SERIAL PRIMARY KEY,
    preparation VARCHAR(255) NOT NULL,
    application_method VARCHAR(100) NOT NULL,
    dose VARCHAR(25) NOT NULL,
    frequency VARCHAR(25) NOT NULL
);


CREATE TABLE IF NOT EXISTS sickness
(
    id SERIAL PRIMARY KEY,
    user_id INTEGER,
    description TEXT NOT NULL,
    diagnosis VARCHAR(255) NOT NULL,
    date_of_beginning DATE NOT NULL,
    date_of_ending DATE,
    prescription_id BIGINT,

    CONSTRAINT fk_sickness_user_id
        FOREIGN KEY (user_id)
            REFERENCES users (id),

    CONSTRAINT fk_sickness_prescription_id
        FOREIGN KEY (prescription_id)
            REFERENCES prescription (id)
);

CREATE TABLE IF NOT EXISTS reception_queue
(
    id SERIAL PRIMARY KEY,
    date DATE NOT NULL,
    start TIME NOT NULL,
    end_time TIME NOT NULL,
    free_time BIGINT NOT NULL,
    day VARCHAR(10) NOT NULL,
    doctor_id INT NOT NULL,

    CONSTRAINT fk_reception_q_doctor_id
        FOREIGN KEY (doctor_id)
            REFERENCES doctor (id)
);

CREATE TABLE IF NOT EXISTS reception_request
(
    id SERIAL PRIMARY KEY,
    time_of_reception TIME NOT NULL,
    reception_duration BIGINT NOT NULL,
    short_description VARCHAR(255),
    reception_queue_id INT NOT NULL,
    user_id INT NOT NULL,

    CONSTRAINT fk_reception_queue_id
        FOREIGN KEY (reception_queue_id)
            REFERENCES reception_queue (id),

    CONSTRAINT fk_reception_r_user_id
        FOREIGN KEY (user_id)
            REFERENCES users (id)
);