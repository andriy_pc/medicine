package model;

import lombok.*;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;

@Entity
@Table(name = "schedule")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Schedule implements Comparable<Schedule> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private long id;

    @Column(name = "day")
    @Enumerated(value = EnumType.STRING)
    private DayOfWeek day;

    @Column(name = "start_time")
    @EqualsAndHashCode.Exclude
//    @ToString.Exclude
    private LocalTime startTime;

    @Column(name = "end_time")
    @EqualsAndHashCode.Exclude
//    @ToString.Exclude
    private LocalTime endTime;

    @Column(name = "break_start_time")
    @EqualsAndHashCode.Exclude
//    @ToString.Exclude
    private LocalTime breakStartTime;

    @Column(name = "break_end_time")
    @EqualsAndHashCode.Exclude
//    @ToString.Exclude
    private LocalTime breakEndTime;

    @Override
    public int compareTo(Schedule o) {
        return o.getDay().getValue() - day.getValue();
    }
}
