package model;

import converter.ProblemConverter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "reception_request")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReceptionRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "time_of_reception")
    private LocalTime timeOfReception;

    @Column(name = "reception_duration")
    @Convert(converter = ProblemConverter.class)
    private Problem receptionDuration;

    @Column(name = "short_description")
    private String shortDescription;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(
            fetch = FetchType.LAZY,
            cascade = {CascadeType.REMOVE}
    )
    @JoinColumn(name = "reception_queue_id")
    private ReceptionQueue queue;
}
