package model;

import lombok.*;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Table(name = "reception_queue")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReceptionQueue {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @EqualsAndHashCode.Exclude
    private long id;

    @Column(name = "date")
    private LocalDate date;

    @Column(name = "start")
    private LocalTime start;

    @Column(name = "end_time")
    private LocalTime end;

    @Column(name = "free_time")
    private Duration freeTime;

    @Column(name = "day")
    @Enumerated(value = EnumType.STRING)
    private DayOfWeek day;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    Doctor doctor;
}
