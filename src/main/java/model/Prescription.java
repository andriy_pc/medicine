package model;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "prescription")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Prescription {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "preparation")
    private String preparation;

    @Column(name = "application_method")
    private String applicationMethod;

    @Column(name = "dose")
    private String dose;

    @Column(name = "frequency")
    private String frequency;
}
