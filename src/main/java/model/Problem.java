package model;

import java.time.Duration;

public enum Problem {
    MEDICAL_EXAMINATION(Duration.ofMinutes(15)),
    CHECKUP(Duration.ofMinutes(25)),
    CONSULTATION(Duration.ofMinutes(35)),
    WHOLE_LIFE_CHECK(Duration.ofHours(72));

    private Duration duration;

    Problem(Duration duration) {
        this.duration = duration;
    }

    public Duration getDuration() {
        return duration;
    }

    public String toString() {
        return "duration: "+duration;
    }
}
