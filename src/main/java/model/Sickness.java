package model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "sickness")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Sickness {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY,
    cascade = CascadeType.REMOVE)
    @JoinColumn(name = "user_id")
    private User user;

    @Column(name = "description")
    @Lob
    private String description;

    @Column(name = "diagnosis")
    private String diagnosis;

    @OneToOne(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST,
                    CascadeType.MERGE,
                    CascadeType.REMOVE}
    )
    @JoinColumn(name = "prescription_id")
    private Prescription prescription;

    @Column(name = "date_of_beginning")
    private LocalDate dateOfBeginning;

    @Column(name = "date_of_ending")
    private LocalDate dateOfEnding;
}
