package config;

import dto.*;
import dto.main.*;
import lombok.RequiredArgsConstructor;
import mapper.*;
import mapper.converter.DurationConverter;
import mapper.converter.StringLocalDateConverter;
import mapper.converter.StringLocalTimeConverter;
import model.*;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
@ComponentScan(basePackages = {"mapper", "converter"})
public class MapperConfig {

    private final UserDtoMapper userDtoMapper;
    private final MainUserDtoMapper mainUserDtoMapper;
    private final DoctorDtoMapper doctorDtoMapper;
    private final MainDoctorDtoMapper mainDoctorDtoMapper;
    private final SicknessDtoMapper sicknessDtoMapper;
    private final MainSicknessDtoMapper mainSicknessDtoMapper;
    private final PrescriptionDtoMapper prescriptionDtoMapper;
    private final MainPrescriptionDtoMapper mainPrescriptionDtoMapper;
    private final ReceptionQueueDtoMapper receptionQueueDtoMapper;
    private final ReceptionRequestDtoMapper receptionRequestDtoMapper;
    private final MainReceptionRequestDtoMapper mainReceptionRequestDtoMapper;
    private final ScheduleDtoMapper scheduleDtoMapper;
    private final MainScheduleDtoMapper mainScheduleDtoMapper;
    private final MainReceptionQueueDtoMapper mainReceptionQueueDtoMapper;

    private final DurationConverter durationConverter;
    private final StringLocalDateConverter localDateConverter;
    private final StringLocalTimeConverter stringLocalTimeConverter;

    @Bean
    ModelMapper modelMapper() {
        ModelMapper mapper = new ModelMapper();

        mapper.addConverter(durationConverter);
        mapper.addConverter(localDateConverter);
        mapper.addConverter(stringLocalTimeConverter);

        mapper.createTypeMap(UserDto.class, User.class)
                .addMappings(userDtoMapper);
        mapper.createTypeMap(User.class, MainUserDto.class)
                .addMappings(mainUserDtoMapper);
        mapper.createTypeMap(DoctorDto.class, Doctor.class)
                .addMappings(doctorDtoMapper);
        mapper.createTypeMap(Doctor.class, MainDoctorDto.class)
                .addMappings(mainDoctorDtoMapper);
        mapper.createTypeMap(SicknessDto.class, Sickness.class)
                .addMappings(sicknessDtoMapper);
        mapper.createTypeMap(Sickness.class, MainSicknessDto.class)
                .addMappings(mainSicknessDtoMapper);
        mapper.createTypeMap(PrescriptionDto.class, Prescription.class)
                .addMappings(prescriptionDtoMapper);
        mapper.createTypeMap(Prescription.class, MainPrescriptionDto.class)
                .addMappings(mainPrescriptionDtoMapper);
        mapper.typeMap(ReceptionQueueDto.class, ReceptionQueue.class)
                .addMappings(receptionQueueDtoMapper);
        mapper.createTypeMap(ReceptionQueue.class, MainReceptionQueueDto.class)
                .addMappings(mainReceptionQueueDtoMapper);
        mapper.createTypeMap(ReceptionRequestDto.class, ReceptionRequest.class)
                .addMappings(receptionRequestDtoMapper);
        mapper.createTypeMap(ReceptionRequest.class, MainReceptionRequestDto.class)
                .addMappings(mainReceptionRequestDtoMapper);
        mapper.createTypeMap(ScheduleDto.class, Schedule.class)
                .addMappings(scheduleDtoMapper);
        mapper.createTypeMap(Schedule.class, MainScheduleDto.class)
                .addMappings(mainScheduleDtoMapper);

        return mapper;
    }
}
