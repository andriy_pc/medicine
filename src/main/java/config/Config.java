package config;

import org.springframework.context.annotation.*;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;


@Configuration
@Import(value = {MapperConfig.class})
@ComponentScan(basePackages = {"model", "service", "dao", "controller"})
@EnableWebMvc
@EnableTransactionManagement
public class Config {

    @Bean
    EntityManagerFactory entityManagerFactory() {
        return Persistence.createEntityManagerFactory("medicine-hibernate-jpa");
    }

    @Bean
    EntityManager entityManager() {
        return entityManagerFactory().createEntityManager();
    }

    @Bean
    JpaTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory());
    }
}
