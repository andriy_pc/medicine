package dto;

import lombok.*;
import model.Gender;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String firstName;
    private String secondName;
    private String dateOfBirth;
    private Gender gender;
    private String phoneNumber;
    private String email;
}
