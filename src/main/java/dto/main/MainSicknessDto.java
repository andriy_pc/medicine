package dto.main;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.User;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainSicknessDto {
    private long id;
    private MainUserDto user;
    private String description;
    private String diagnosis;
    private MainPrescriptionDto prescription;
    private LocalDate dateOfBeginning;
    private LocalDate dateOfEnding;
}
