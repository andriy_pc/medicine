package dto.main;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.Day;
import model.Schedule;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainScheduleDto implements Comparable<MainScheduleDto> {
    private long id;
    private DayOfWeek day;
    private String startTime;
    private String endTime;
    private String breakStartTime;
    private String breakEndTime;

    @Override
    public int compareTo(MainScheduleDto o) {
        return o.getDay().getValue() - day.getValue();
    }
}
