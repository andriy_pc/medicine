package dto.main;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainPrescriptionDto {
    private long id;
    private String preparation;
    private String applicationMethod;
    private String dose;
    private String frequency;
}