package dto.main;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.Gender;
import model.Schedule;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainDoctorDto {
    private long id;
    private String firstName;
    private String secondName;
    private String dateOfBirth;
    private Gender gender;
    private String phoneNumber;
    private String email;
    private String specialization;
    private Set<MainScheduleDto> schedules;
}
