package dto.main;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.Gender;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainUserDto {
    private long id;
    private String firstName;
    private String secondName;
    private String dateOfBirth;
    private Gender gender;
    private String phoneNumber;
    private String email;
    private String dateOfRegistration;
}
