package dto.main;

import lombok.*;
import model.Problem;
import model.ReceptionQueue;

import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainReceptionRequestDto {
    @EqualsAndHashCode.Exclude
    private long id;
    private LocalTime receptionStart;
    private LocalTime receptionEnd;
    private Problem problem;
    private String userFirstName;
    private String userSecondName;
    private MainReceptionQueueDto queue;
}
