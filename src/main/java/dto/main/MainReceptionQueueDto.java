package dto.main;

import lombok.*;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MainReceptionQueueDto {

    @EqualsAndHashCode.Exclude
    private long id;

    private LocalDate date;

    private DayOfWeek day;

    private LocalTime start;

    private LocalTime end;

    private Duration freeTime;

    private MainDoctorDto doctor;

    @EqualsAndHashCode.Exclude
    private List<MainReceptionRequestDto> requests;
}
