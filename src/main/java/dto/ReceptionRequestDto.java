package dto;

import dto.main.MainReceptionQueueDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.Problem;
import model.User;

import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReceptionRequestDto {
    private LocalTime timeOfReception;
    private Problem receptionDuration;
    private String shortDescription;
    private User user;
    private MainReceptionQueueDto queue;
}
