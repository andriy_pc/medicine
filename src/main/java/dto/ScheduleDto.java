package dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ScheduleDto implements Comparable<ScheduleDto> {
    private DayOfWeek day;
    private String startTime;
    private String endTime;
    private String breakStartTime;
    private String breakEndTime;

    @Override
    public int compareTo(ScheduleDto o) {
        return o.getDay().getValue() - day.getValue();
    }
}
