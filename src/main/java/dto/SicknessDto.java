package dto;

import dto.main.MainPrescriptionDto;
import dto.main.MainUserDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SicknessDto {
    private MainUserDto user;
    private String description;
    private String diagnosis;
    private PrescriptionDto prescription;
    private LocalDate dateOfBeginning;
    private LocalDate dateOfEnding;
}
