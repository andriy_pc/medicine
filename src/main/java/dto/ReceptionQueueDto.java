package dto;

import dto.main.MainDoctorDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import model.Day;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReceptionQueueDto {
    private LocalDate date;
    private MainDoctorDto doctor;
}
