package mapper.converter;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

import java.time.DayOfWeek;
import java.time.LocalDate;

@Component
public class DayOfWeekConverter implements Converter<LocalDate, DayOfWeek> {
    @Override
    public DayOfWeek convert(MappingContext<LocalDate, DayOfWeek> context) {
        return context.getSource().getDayOfWeek();
    }
}
