package mapper.converter;

import model.Problem;
import org.modelmapper.AbstractConverter;
import org.springframework.stereotype.Component;

import java.time.Duration;

/**Converter for ModelMapper*/
@Component
public class DurationConverter extends AbstractConverter<Problem, Duration> {
    @Override
    protected Duration convert(Problem problem) {
        return problem.getDuration();
    }
}
