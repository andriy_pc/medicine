package mapper;

import dto.ReceptionRequestDto;
import model.ReceptionRequest;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class ReceptionRequestDtoMapper extends PropertyMap<ReceptionRequestDto, ReceptionRequest> {
    @Override
    protected void configure() {
        map().setReceptionDuration(source.getReceptionDuration());
    }
}
