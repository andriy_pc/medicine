package mapper;

import dto.main.MainSicknessDto;
import model.Sickness;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class MainSicknessDtoMapper extends PropertyMap<Sickness, MainSicknessDto> {
    @Override
    protected void configure() {

    }
}
