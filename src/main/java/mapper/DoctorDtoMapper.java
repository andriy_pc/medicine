package mapper;

import dto.DoctorDto;
import model.Doctor;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class DoctorDtoMapper extends PropertyMap<DoctorDto, Doctor> {
    @Override
    protected void configure() {

    }
}
