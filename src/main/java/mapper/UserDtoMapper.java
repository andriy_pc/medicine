package mapper;

import dto.UserDto;
import model.User;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class UserDtoMapper extends PropertyMap<UserDto, User> {

    @Override
    protected void configure() {
        map().setDateOfRegistration(LocalDate.now());
    }
}
