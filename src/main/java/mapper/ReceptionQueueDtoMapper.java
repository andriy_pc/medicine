package mapper;

import dto.ReceptionQueueDto;
import lombok.Data;
import mapper.converter.DayOfWeekConverter;
import model.ReceptionQueue;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
@Data
public class ReceptionQueueDtoMapper extends PropertyMap<ReceptionQueueDto, ReceptionQueue> {

    private final DayOfWeekConverter dayOfWeekConverter;

    @Override
    protected void configure() {
        using(dayOfWeekConverter).map(source.getDate(), destination.getDay());
    }
}
