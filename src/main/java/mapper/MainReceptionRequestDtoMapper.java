package mapper;

import dto.main.MainReceptionRequestDto;
import model.ReceptionRequest;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class MainReceptionRequestDtoMapper extends PropertyMap<ReceptionRequest, MainReceptionRequestDto> {
    @Override
    protected void configure() {

        map().setUserFirstName(source.getUser().getFirstName());
        map().setUserSecondName(source.getUser().getSecondName());
        map().setReceptionStart(source.getTimeOfReception());
        map().setProblem(source.getReceptionDuration());
        skip().setReceptionEnd(null);
    }
}
