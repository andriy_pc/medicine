package mapper;

import dto.main.MainScheduleDto;
import model.Schedule;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class MainScheduleDtoMapper extends PropertyMap<Schedule, MainScheduleDto> {
    @Override
    protected void configure() {
    }
}
