package mapper;

import dto.PrescriptionDto;
import model.Prescription;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class PrescriptionDtoMapper extends PropertyMap<PrescriptionDto, Prescription> {
    @Override
    protected void configure() {

    }
}
