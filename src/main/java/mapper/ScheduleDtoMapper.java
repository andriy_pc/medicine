package mapper;

import dto.ScheduleDto;
import model.Schedule;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import java.time.LocalTime;

@Component
public class ScheduleDtoMapper extends PropertyMap<ScheduleDto, Schedule> {
    @Override
    protected void configure() {
    }
}
