package mapper;

import dto.main.MainUserDto;
import org.modelmapper.PropertyMap;
import model.User;
import org.springframework.stereotype.Component;

@Component
public class MainUserDtoMapper extends PropertyMap<User, MainUserDto> {

    @Override
    protected void configure() {
    }
}
