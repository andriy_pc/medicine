package mapper;

import dto.main.MainDoctorDto;
import model.Doctor;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class MainDoctorDtoMapper extends PropertyMap<Doctor, MainDoctorDto> {
    @Override
    protected void configure() {
    }
}
