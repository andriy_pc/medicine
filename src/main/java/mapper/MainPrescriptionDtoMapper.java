package mapper;

import dto.main.MainPrescriptionDto;
import model.Prescription;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class MainPrescriptionDtoMapper extends PropertyMap<Prescription, MainPrescriptionDto> {
    @Override
    protected void configure() {

    }
}
