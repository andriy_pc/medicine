package mapper;

import dto.SicknessDto;
import model.Sickness;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

@Component
public class SicknessDtoMapper extends PropertyMap<SicknessDto, Sickness> {
    @Override
    protected void configure() {

    }
}
