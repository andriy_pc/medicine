package mapper;

import dto.main.MainReceptionQueueDto;
import model.ReceptionQueue;
import model.Schedule;
import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
public class MainReceptionQueueDtoMapper
        extends PropertyMap<ReceptionQueue, MainReceptionQueueDto> {

    @Override
    protected void configure() {
        map().setRequests(new ArrayList<>());
    }
}
