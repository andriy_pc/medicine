package converter;

import model.Problem;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ProblemConverter implements AttributeConverter<Problem, Long> {
    @Override
    public Long convertToDatabaseColumn(Problem problem) {
        return problem.getDuration().toMinutes();
    }

    @Override
    public Problem convertToEntityAttribute(Long duration) {
        switch (duration.intValue()) {
            case 15: {
                return Problem.MEDICAL_EXAMINATION;
            }
            case 25: {
                return Problem.CHECKUP;
            }
            case 35: {
                return Problem.CONSULTATION;
            }
            default:
                return Problem.CONSULTATION;
        }
    }
}
