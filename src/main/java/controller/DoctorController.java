package controller;

import dto.DoctorDto;
import dto.main.MainDoctorDto;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import service.DoctorService;

import java.util.List;

@RestController
@RequestMapping(value = "/doctors")
@Data
public class DoctorController {

    private final DoctorService service;

    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MainDoctorDto getDoctorById(@PathVariable long id) {
        return service.findById(id);
    }

    @GetMapping
    public List<MainDoctorDto> findAllDoctors() {
        return service.findAll();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MainDoctorDto saveDoctor(@RequestBody DoctorDto doctordto) {
        return service.save(doctordto);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MainDoctorDto updateDoctor(@PathVariable long id,
                                      @RequestBody DoctorDto doctordto) {
        return service.update(id, doctordto);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteDoctor(@PathVariable long id) {
        service.delete(id);
    }
}
