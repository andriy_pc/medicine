package controller;

import dto.UserDto;
import dto.main.MainUserDto;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import service.UserService;

import java.util.List;

@RestController
@RequestMapping(value = "/users")
@Data
public class UserController {

    private final UserService userService;

    @GetMapping(value = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public MainUserDto getUserById(@PathVariable long id) {
        return userService.findById(id);
    }

    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    public List<MainUserDto> getAllUsers() {
        return userService.getAll();
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public MainUserDto saveUser(@RequestBody UserDto userDto) {
        return userService.save(userDto);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public MainUserDto updateUser(@PathVariable long id,
                                  @RequestBody UserDto newUserDto) {
        return userService.update(id, newUserDto);
    }

    @DeleteMapping(value = "/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable long id) {
        userService.delete(id);
    }

}
