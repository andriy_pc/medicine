package exceptions;

public class NoFreeTimeException extends RuntimeException {

    public NoFreeTimeException() {
    }

    public NoFreeTimeException(String message) {
        super(message);
    }
}
