package dao;

import lombok.Data;
import model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Data
public class UserDao {

    @PersistenceContext
    private EntityManager entityManager;

    public User save(User user) {
        entityManager.persist(user);
        return user;
    }

    public User update(User newUser) {
        entityManager.merge(newUser);
        return newUser;
    }

    public User find(long id) {
        return entityManager.find(User.class, id);
    }

    public void remove(long id) {
        User userToRemove = find(id);
//        entityManager.remove(userToRemove);
        entityManager.createQuery("DELETE FROM User " +
                "WHERE id = :id")
        .setParameter("id", id)
        .executeUpdate();
    }

    public List<User> getAll() {
        return entityManager.createQuery("SELECT u FROM User u", User.class).getResultList();
    }
}
