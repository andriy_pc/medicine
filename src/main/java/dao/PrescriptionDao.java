package dao;

import model.Prescription;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PrescriptionDao {

    private static Logger log = LogManager.getLogger(PrescriptionDao.class);

    @PersistenceContext
    EntityManager entityManager;

    public Prescription save(Prescription prescription) {
        log.info("Saving prescription: {}", prescription);
        entityManager.persist(prescription);
        return prescription;
    }

    public Prescription findById(long id) {
        log.info("Looking for prescription with id: {}", id);
        return entityManager.find(Prescription.class, id);
    }

    public Prescription findBySicknessId(long id) {
        log.info("Looking for all prescriptions with sickness id: {}", id);
        return entityManager.createQuery("SELECT p FROM Prescription p " +
                "WHERE sickness_id = :id", Prescription.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public Prescription update(Prescription prescription) {
        log.info("Updating prescription with id: {}", prescription.getId());
        entityManager.merge(prescription);
        return prescription;
    }

    public void remove(Prescription prescription) {
        log.info("Removing prescription with id: {}", prescription.getId());
        entityManager.remove(prescription);
    }

    public void removeBySicknessId(long id) {
        log.info("Removing prescription by sickness id: {}", id);
        entityManager.createQuery("DELETE FROM Prescription " +
                "WHERE sickness_id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

}
