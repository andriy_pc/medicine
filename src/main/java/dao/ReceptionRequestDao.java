package dao;

import model.ReceptionRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Repository
public class ReceptionRequestDao {

    private static Logger log = LogManager.getLogger(ReceptionRequestDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public ReceptionRequest save(ReceptionRequest request) {
        entityManager.persist(request);
        log.info("Saved reception request with id: {}", request.getId());
        return request;
    }

    public ReceptionRequest findById(long id) {
        log.info("Looking for reception request with id: {}", id);
        return entityManager.createQuery("SELECT r FROM ReceptionRequest r " +
                "left outer join fetch r.queue " +
                "WHERE r.id = :id", ReceptionRequest.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public Optional<List<ReceptionRequest>> findByQueueId(long id) {
        log.info("Looking for reception requests with queue id: {}", id);
        return Optional.ofNullable(
                entityManager.createQuery("SELECT r FROM ReceptionRequest r " +
                        "WHERE reception_queue_id = :id", ReceptionRequest.class)
                        .setParameter("id", id).getResultList()
        );
    }

    public ReceptionRequest update(ReceptionRequest request) {
        log.info("Updating reception request with id: {}", request.getId());
        entityManager.merge(request);
        return request;
    }

    public void remove(ReceptionRequest request) {
        log.info("Removing reception request with id: {}", request.getId());
        entityManager.remove(request);
    }

}
