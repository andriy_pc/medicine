package dao;

import org.springframework.stereotype.Component;

@Component
public interface SimpleDao<T> {
    public T save(T t);
    public T update(T t);
    public T find(long id);
    public void remove(long id);
}
