package dao;

import model.Sickness;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class SicknessDao {

    private static Logger log = LogManager.getLogger(SicknessDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Sickness save(Sickness sickness) {
        log.info("saving sickness with id: {}", sickness.getId());
        entityManager.persist(sickness);
        log.info("Saved sickness with id: {}", sickness.getId());
        return sickness;
    }

    public Sickness update(Sickness sickness) {
        log.info("Updating sickness with id: {}", sickness.getId());
        entityManager.merge(sickness);
        return sickness;
    }

    public Sickness findById(long id) {
        log.info("Looking for sickness with id: {}", id);
        return entityManager.createQuery("SELECT s FROM Sickness s " +
                "LEFT OUTER JOIN FETCH s.prescription " +
                "LEFT OUTER JOIN FETCH s.user " +
                "WHERE s.id = :id", Sickness.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public List<Sickness> findAllByUserId(long userId) {
        return entityManager.createQuery("SELECT s FROM Sickness s " +
                "LEFT OUTER JOIN FETCH s.prescription " +
                "LEFT OUTER JOIN FETCH s.user " +
                "WHERE user_id = :id", Sickness.class)
                .setParameter("id", userId)
                .getResultList();
    }

    public void remove(Sickness sickness) {
        entityManager.remove(sickness);
    }

    public void removeByUserId(long userId) {
        entityManager.createQuery("DELETE FROM Sickness " +
                "WHERE user_id =: id")
                .setParameter("id", userId)
                .executeUpdate();
    }

    public void removeByUserId(List<Sickness> sicknesses) {
        sicknesses.stream().forEach(this::remove);
    }
}
