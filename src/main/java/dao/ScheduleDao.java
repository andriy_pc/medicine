package dao;

import model.Schedule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Repository
public class ScheduleDao {

    private static Logger log = LogManager.getLogger(ScheduleDao.class);

    @PersistenceContext
    EntityManager entityManager;


    public Schedule save(Schedule schedule) {
        entityManager.persist(schedule);
        log.info("Saved schedule with id: {}", schedule.getId());
        return schedule;
    }

    public Schedule findById(long id) {
        log.info("Looking for schedule with id: {}", id);
        return entityManager.find(Schedule.class, id);
    }

    public Schedule update(Schedule newSchedule) {
        log.info("Updating schedule with id: {}", newSchedule.getId());
        entityManager.merge(newSchedule);
        return newSchedule;
    }

    public void remove(Schedule schedule) {
        log.info("Removing schedule with id: {}", schedule.getId());
        entityManager.remove(schedule);
    }

}
