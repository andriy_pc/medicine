package dao;

import model.Doctor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class DoctorDao {

    private static Logger log = LogManager.getLogger(DoctorDao.class);

    @PersistenceContext
    private EntityManager entityManager;

    public Doctor save(Doctor doctor) {
        log.info("Saving doctor: {}", doctor);
        entityManager.persist(doctor);
        return doctor;
    }

    public Doctor findById(long id) {
        log.info("Looking for doctor with id: {}", id);
        return entityManager
                .createQuery("SELECT d FROM Doctor d " +
                        "left outer join fetch d.schedules " +
                        "WHERE d.id = :id", Doctor.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    public Doctor update(Doctor newDoctor) {
        log.info("Updating doctor with id: {}", newDoctor.getId());
        entityManager.merge(newDoctor);
        return newDoctor;
    }

    public void remove(Doctor docToDelete) {
        log.info("Removing doctor with id: {}", docToDelete.getId());
        entityManager.remove(docToDelete);
    }

    public List<Doctor> getAll() {
        return entityManager.createQuery("SELECT d FROM Doctor d",
                Doctor.class).getResultList();
    }
}
