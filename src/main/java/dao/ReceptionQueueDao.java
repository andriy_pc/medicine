package dao;

import model.ReceptionQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.time.LocalDate;
import java.util.List;

@Repository
public class ReceptionQueueDao {

    private static Logger log = LogManager.getLogger(ReceptionQueueDao.class);

    @PersistenceContext
    EntityManager entityManager;

    public ReceptionQueue save(ReceptionQueue queue) {
        entityManager.persist(queue);
        log.info("Saved reception queue with id: {}", queue.getId());
        return queue;
    }

    public ReceptionQueue findById(long id) {
        log.info("Looking for ReceptionQueue with id: {}", id);
        return entityManager.find(ReceptionQueue.class, id);
    }

    public List<ReceptionQueue> findAllByDateForDoctor(LocalDate date, long doctorId) {
        log.info("Getting all reception queue where date: {} " +
                "and doctor id: {}", date, doctorId);
        return entityManager.createQuery("SELECT q FROM ReceptionQueue q " +
                "left outer join fetch q.doctor d " +
                "WHERE date = :date AND doctor_id = :id" +
                "", ReceptionQueue.class)
                .setParameter("date", date)
                .setParameter("id", doctorId)
                .getResultList();
    }

    public ReceptionQueue update(ReceptionQueue queue) {
        ReceptionQueue updateResult = entityManager.merge(queue);
        log.info("Updated reception queue with id: {}", updateResult.getId());
        return queue;
    }

    public void remove(ReceptionQueue queue) {
        log.info("Removing reception queue with id: {}", queue.getId());
        entityManager.remove(queue);
    }

    public ReceptionQueue merge(ReceptionQueue queue) {
        log.info("Merging reception queue with id: {}", queue.getId());
        ReceptionQueue result = entityManager.merge(queue);
        log.info("Check if entity is managed: {}", entityManager.contains(result));
        return result;
    }

}
