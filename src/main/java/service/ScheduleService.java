package service;

import dao.ScheduleDao;
import dto.ScheduleDto;
import dto.main.MainScheduleDto;
import exceptions.NoSuchEntityException;
import lombok.Data;
import model.Schedule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Data
public class ScheduleService {
    private static Logger log = LogManager.getLogger(ScheduleService.class);

    private final ScheduleDao scheduleDao;
    private final ModelMapper mapper;

    @Transactional
    public MainScheduleDto save(ScheduleDto scheduleDto) {
        log.info("Saving schedule: {}", scheduleDto);
        Schedule schedule = mapper.map(scheduleDto, Schedule.class);
        return mapper.map(scheduleDao.save(schedule), MainScheduleDto.class);
    }

    public MainScheduleDto findById(long id) {
        log.info("Looking for schedule with id: {}", id);
        return mapper.map(find(id), MainScheduleDto.class);
    }

    @Transactional
    public MainScheduleDto update(long id, ScheduleDto scheduleDto) {
        log.info("Updating Schedule with id: {}", id);
        Schedule schedule = mapper.map(scheduleDto, Schedule.class);
        schedule.setId(id);
        return mapper.map(scheduleDao.update(schedule), MainScheduleDto.class);
    }

    @Transactional
    public void delete(long id) {
        log.info("Deleting schedule with id: {}", id);
        Schedule schedule = find(id);
        scheduleDao.remove(schedule);
    }

    private Schedule find(long id) {
        log.info("Find schedule with id: {}", id);
        Schedule result = scheduleDao.findById(id);
        if (result == null) {
            log.error("There is no Schedule with id: {}", id);
            throw new NoSuchEntityException("There is no Schedule with id = " + id);
        } else {
            log.info("Found schedule with id: {}", id);
            return result;
        }
    }
}
