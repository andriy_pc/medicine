package service;

import dao.UserDao;
import dto.UserDto;
import dto.main.MainUserDto;
import exceptions.NoSuchEntityException;
import lombok.Data;
import model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class UserService {

    private static Logger log = LogManager.getLogger(UserService.class);

    private final UserDao userDao;
    private final ModelMapper mapper;
    private final SicknessService sicknessService;

    @Transactional
    public MainUserDto save(UserDto userDto) {
        log.info("Saving user with name: {}", userDto.getFirstName());
        User user = mapper.map(userDto, User.class);
        return mapper.map(userDao.save(user), MainUserDto.class);
    }

    @Transactional
    public MainUserDto findById(long id) {
        log.info("Looking for user with id: {}", id);
        User result = userDao.find(id);
        if(result == null) {
            throw new NoSuchEntityException("User with id = " + id + " was not found");
        }
        return mapper.map(result, MainUserDto.class);
    }

    @Transactional
    public MainUserDto update(long oldUserId, UserDto newUserDto) {
        log.info("Verification existing of user with id = {}", oldUserId);
        findById(oldUserId);
        log.info("Updating user with id: {}", oldUserId);
        User newUser = mapper.map(newUserDto, User.class);
        newUser.setId(oldUserId);
        return mapper.map(userDao.update(newUser), MainUserDto.class);
    }

    @Transactional
    public void delete(long id) {
        log.info("Deleting user with id: {}", id);
        sicknessService.deleteAllByUserId(id);
    }

    @Transactional
    public List<MainUserDto> getAll() {
        return userDao.getAll().stream()
                .map(u -> mapper.map(u, MainUserDto.class))
                .collect(Collectors.toList());
    }
}
