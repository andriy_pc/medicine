package service;

import dao.DoctorDao;
import dto.DoctorDto;
import dto.main.MainDoctorDto;
import exceptions.NoSuchEntityException;
import lombok.RequiredArgsConstructor;
import model.Doctor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DoctorService {

    private static Logger log = LogManager.getLogger(DoctorService.class);

    private final DoctorDao doctorDao;
    private final ModelMapper mapper;
    private final ScheduleService scheduleService;

    @Transactional
    public MainDoctorDto save(DoctorDto doctorDto) {
        log.info("Saving doctor: {}", doctorDto);
        Doctor result = doctorDao.save(mapper.map(doctorDto, Doctor.class));
        return mapper.map(result, MainDoctorDto.class);
    }

    @Transactional
    public MainDoctorDto findById(long id) {
        log.info("Looking for Doctor with id: {}", id);
        Doctor result = find(id);
        return mapper.map(result, MainDoctorDto.class);
    }

    @Transactional
    public MainDoctorDto update(long id, DoctorDto newDoctor) {
        log.info("Updating doctor with id: {}", id);
        find(id);
        Doctor doctor = mapper.map(newDoctor, Doctor.class);
        doctor.setId(id);
        doctorDao.update(doctor);
        return mapper.map(doctor, MainDoctorDto.class);
    }

    @Transactional
    public void delete(long id) {
        log.info("Deleting doctor with id: {}", id);
        Doctor docToDelete = find(id);
        doctorDao.remove(docToDelete);
    }

    public List<MainDoctorDto> findAll() {
        return doctorDao.getAll().stream()
                .map(d -> mapper.map(d, MainDoctorDto.class))
                .collect(Collectors.toList());
    }

    private Doctor find(long id) {
        log.info("Find doctor with id: {}", id);
        try {
            Doctor doctor = doctorDao.findById(id);
            log.info("Doctor was found with id: {}", id);
            return doctor;
        } catch (NoResultException e) {
            log.error("Doctor was not found with id: {}", id);
            throw new NoSuchEntityException("There is no doctor with id = " + id);
        }

    }

}
