package service;

import dao.SicknessDao;
import dto.SicknessDto;
import dto.main.MainSicknessDto;
import exceptions.NoSuchEntityException;
import lombok.RequiredArgsConstructor;
import model.Sickness;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SicknessService {

    private static Logger log = LogManager.getLogger(SicknessService.class);

    private final SicknessDao sicknessDao;
    private final ModelMapper mapper;
    private final PrescriptionService prescriptionService;

    @Transactional
    public MainSicknessDto save(SicknessDto sicknessDto) {
        log.info("saving sickness: {}", sicknessDto);
        Sickness sickness = sicknessDao.save(mapper.map(sicknessDto, Sickness.class));
        return mapper.map(sickness, MainSicknessDto.class);
    }

    @Transactional
    public MainSicknessDto update(long id, SicknessDto newSicknessDto) {
        log.info("Updating sickness with id {}", id);
        Sickness oldSickness = find(id);
        mapper.map(newSicknessDto, oldSickness);
        return mapper.map(oldSickness, MainSicknessDto.class);
    }


    public MainSicknessDto findById(long id) {
        log.info("Looking for sickness with id: {}", id);
        Sickness sickness = find(id);
        return mapper.map(sickness, MainSicknessDto.class);
    }

    public List<MainSicknessDto> findAllByUserId(long userId) {
        List<Sickness> sicknesses = sicknessDao.findAllByUserId(userId);
        if (!sicknesses.isEmpty()) {
            log.info("For user with id: {} " +
                    "found sicknesses: {}", userId, sicknesses.size());
            return sicknesses.stream()
                    .map(s -> mapper.map(s, MainSicknessDto.class))
                    .collect(Collectors.toList());
        } else {
            log.error("There is no sicknesses for user with id: {}", userId);
            throw new NoSuchEntityException(
                    "There is no sicknesses for user with id = " + userId);
        }
    }

    @Transactional
    public void delete(long id) {
        log.info("Deleting sickness with id: {}", id);
        Sickness sickness = find(id);
        sicknessDao.remove(sickness);
    }

    @Transactional
    public void deleteAllByUserId(long userId) {
        log.info("Deleting all sickness with userId: {}", userId);
        List<Sickness> sicknesses = sicknessDao.findAllByUserId(userId);
        sicknessDao.removeByUserId(sicknesses);
    }

    private Sickness find(long id) {
        log.info("find Sickness with id: {}", id);
        try {
            Sickness result = sicknessDao.findById(id);
            log.info("Found sickness with id: {}", id);
            return result;
        } catch (NoResultException e) {
            log.error("Sickness with id = {} was not found!", id);
            throw new NoSuchEntityException("There is no Sickness with id = " + id);
        }
    }

}
