package service;

import dao.PrescriptionDao;
import dto.PrescriptionDto;
import dto.main.MainPrescriptionDto;
import exceptions.NoSuchEntityException;
import lombok.RequiredArgsConstructor;
import model.Prescription;
import model.Sickness;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PrescriptionService {

    private static Logger log = LogManager.getLogger(PrescriptionService.class);
    private final PrescriptionDao prescriptionDao;
    private final ModelMapper mapper;

    @Transactional
    public MainPrescriptionDto save(PrescriptionDto prescriptionDto) {
        log.info("Saving prescription: {}", prescriptionDto);
        Prescription prescription = mapper.map(prescriptionDto, Prescription.class);
        return mapper.map(prescriptionDao.save(prescription), MainPrescriptionDto.class);
    }

    public MainPrescriptionDto findById(long id) {
        log.info("Looking for prescription with id: {}", id);
        return mapper.map(find(id), MainPrescriptionDto.class);
    }

    public List<MainPrescriptionDto> findAllBySicknessesId(List<Sickness> sicknesses) {
        List<MainPrescriptionDto> result = new ArrayList<>();

        for (Sickness s : sicknesses) {
            try {
                Prescription p = prescriptionDao.findBySicknessId(s.getId());
                result.add(mapper.map(p, MainPrescriptionDto.class));
            } catch (NoResultException e) {
                log.info("NoResultException occurred while getting " +
                        "prescriptions for sickness with id: {}",
                        s.getId());
            }
        }
        return  result;
    }

    @Transactional
    public void deleteBySicknessId(long sicknessId) {
        log.info("Deleting prescription by sickness id: {}", sicknessId);
        prescriptionDao.removeBySicknessId(sicknessId);
    }

    @Transactional
    public MainPrescriptionDto update(long id, PrescriptionDto newPrescription) {
        log.info("Updating prescription with id: {}", id);
        find(id);
        Prescription prescription = mapper.map(newPrescription, Prescription.class);
        prescription.setId(id);

        return mapper.map(prescriptionDao.update(prescription), MainPrescriptionDto.class);
    }

    @Transactional
    public void delete(long id) {
        log.info("Deleting prescription with id: {}", id);
        Prescription prescription = find(id);
        prescriptionDao.remove(prescription);
    }

    private Prescription find(long id) {
        log.info("Find prescription with id: {}", id);
        Prescription result = prescriptionDao.findById(id);
        if (result == null) {
            log.error("There is no prescription with id: {}", id);
            throw new NoSuchEntityException("There is no prescription with id = " + id);
        } else {
            log.info("Found prescription with id: {}", id);
            return result;
        }
    }

}
