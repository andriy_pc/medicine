package service;

import dao.ReceptionQueueDao;
import dto.ReceptionQueueDto;
import dto.main.MainReceptionQueueDto;
import dto.main.MainReceptionRequestDto;
import exceptions.NoSuchEntityException;
import lombok.Data;
import model.ReceptionQueue;
import model.Schedule;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Data
public class ReceptionQueueService {

    private static Logger log = LogManager.getLogger(ReceptionQueueService.class);

    private final ReceptionQueueDao queueDao;
    @Lazy
    private final ReceptionRequestService requestService;
    private final ModelMapper mapper;

    @Transactional
    public MainReceptionQueueDto save(ReceptionQueueDto queue) {
        ReceptionQueue receptionQueue = mapper.map(queue, ReceptionQueue.class);
        log.info("Saved reception queue with id: {}", receptionQueue.getId());

        setStartEndOfReception(receptionQueue);
        receptionQueue.setFreeTime(calculateFreeTime(receptionQueue));

        MainReceptionQueueDto result =
                mapper.map(queueDao.save(receptionQueue), MainReceptionQueueDto.class);

        return result;
    }

    @Transactional
    public MainReceptionQueueDto findById(long id) {
        log.info("Looking for reception queue with id: {}", id);
        ReceptionQueue queue = find(id);
        MainReceptionQueueDto result =
                mapper.map(queue, MainReceptionQueueDto.class);

        result.setRequests(findReceptionRequests(id));
        return result;
    }

    @Transactional
    public List<MainReceptionQueueDto> findAllByDateForDoctor(LocalDate date, long doctorId) {
        log.info("Getting all reception queue where date: {} " +
                "and doctorId: {}", date, doctorId);
        List<ReceptionQueue> queues = queueDao.findAllByDateForDoctor(date, doctorId);
        if (queues == null) {
            throw new NoSuchEntityException("There is now Queries for doctor with id = "
                    + doctorId + "on date: " + date);
        } else {
            return queues.stream()
                    .map(q -> mapper.map(q, MainReceptionQueueDto.class))
                    .peek(mq -> mq.setRequests(findReceptionRequests(mq.getId())))
                    .collect(Collectors.toList());
        }
    }

    @Transactional
    public MainReceptionQueueDto update(long id, ReceptionQueueDto newReceptionQueue) {
        log.info("Updating reception queue with id: {}", id);
        ReceptionQueue oldQueue = find(id);
        ReceptionQueue newQueue = mapper.map(newReceptionQueue, ReceptionQueue.class);
        newQueue.setId(id);

        setStartEndOfReception(newQueue);
        newQueue.setFreeTime(calculateFreeTime(newQueue));

        queueDao.update(newQueue);//TODO remove anti-pattern
        MainReceptionQueueDto result = mapper.map(oldQueue, MainReceptionQueueDto.class);

        result.setRequests(findReceptionRequests(id));

        return result;
    }

    @Transactional
    public void delete(long id) {
        log.info("Deleting reception queue with id: {}", id);
        ReceptionQueue queue = find(id);
        queueDao.remove(queue);
    }

    @Transactional
    public ReceptionQueue merge(ReceptionQueue queue) {
        log.info("Merging reception queue with id: {}", queue.getId());
        return queueDao.merge(queue);
    }

    private ReceptionQueue find(long id) {
        log.info("Find reception queue with id: {}", id);
        ReceptionQueue result = queueDao.findById(id);
        if (result == null) {
            log.error("ReceptionQueue with id: {} was not found!", id);
            throw new NoSuchEntityException("There is no reception queue with id = " + id);
        } else {
            log.info("Reception Queue with id: {} was found!", id);
            return result;
        }
    }

    private List<MainReceptionRequestDto> findReceptionRequests(long id) {
        log.info("Searching for reception request by queue id: {}", id);
        return requestService.findByQueueId(id);
    }

    private void setStartEndOfReception(ReceptionQueue receptionQueue) {
        log.info("Setting start and end of reception queue with id: {}", receptionQueue.getId());

        Schedule schedule = getScheduleByDay(receptionQueue).orElseThrow(
                () -> new NoSuchEntityException("There is no schedule for date: "
                        + receptionQueue.getDate())
        );
        receptionQueue.setStart(schedule.getStartTime());
        receptionQueue.setEnd(schedule.getEndTime());
    }

    private Duration calculateFreeTime(ReceptionQueue queue) {
        log.info("calculating free time for main queue dto with id: {}", queue.getId());

        LocalTime delta = queue.getEnd()
                .minus(queue.getStart().toSecondOfDay(), ChronoUnit.SECONDS);

        List<MainReceptionRequestDto> requests = findReceptionRequests(queue.getId());

        if (requests.size() == 0) {
            return Duration.ofSeconds(delta.toSecondOfDay());
        } else {
            Duration durationOfRequests = requests.stream()
                    .map(req -> req.getProblem().getDuration())
                    .reduce(Duration.ofMinutes(0), Duration::plus);
            log.debug("calculated duration of requests: {}", durationOfRequests);

            LocalTime freeTime = delta.minus(durationOfRequests);
            log.debug("Calculated free time: {}", freeTime);

            return Duration.of(freeTime.toSecondOfDay(), ChronoUnit.SECONDS);
        }
    }

    private Optional<Schedule> getScheduleByDay(ReceptionQueue queue) {
        log.info("Getting schedule by queue with id: {}, for day: {}",
                queue.getId(), queue.getDay());
        DayOfWeek day = queue.getDay();
        Iterator<Schedule> scheduleIterator = queue.getDoctor().getSchedules().iterator();
        while (scheduleIterator.hasNext()) {
            Schedule schedule = scheduleIterator.next();
            if (schedule.getDay().equals(day)) {
                return Optional.of(schedule);
            }
        }
        return Optional.empty();
    }
}
