package service;

import dao.ReceptionRequestDao;
import dto.ReceptionRequestDto;
import dto.main.MainReceptionQueueDto;
import dto.main.MainReceptionRequestDto;
import exceptions.NoFreeTimeException;
import exceptions.NoSuchEntityException;
import lombok.Data;
import model.ReceptionRequest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Data
public class ReceptionRequestService {

    private static Logger log = LogManager.getLogger(ReceptionRequestService.class);

    private final ReceptionRequestDao requestDao;
    private final ModelMapper mapper;

    @Transactional
    public MainReceptionRequestDto save(ReceptionRequestDto requestDto) {
        log.info("Saving reception request");
        ReceptionRequest request = mapper.map(requestDto, ReceptionRequest.class);
        MainReceptionQueueDto queue = requestDto.getQueue();

        if (queue.getFreeTime()
                .compareTo(request.getReceptionDuration().getDuration()) < 0) {
            log.error("Saving request for queue with no enough free time");
            log.error("queue id: {}, free time: {}; request duration: {}", queue.getFreeTime(),
                    queue.getFreeTime(), requestDto.getReceptionDuration().getDuration());
            throw new NoFreeTimeException("There is no free time " +
                    "for date: " + queue.getDate()
                    + ", for doctor with id = " + queue.getDoctor().getId());
        } else {
            MainReceptionRequestDto result =
                    mapper.map(requestDao.save(request), MainReceptionRequestDto.class);
            result = calculateReceptionEnd(result, request);
            return result;
        }

    }

    @Transactional
    public MainReceptionRequestDto findById(long id) {
        log.info("Looking for reception request with id: {}", id);
        ReceptionRequest request = find(id);
        MainReceptionRequestDto result =
                mapper.map(request, MainReceptionRequestDto.class);

        result = calculateReceptionEnd(result, request);
        return result;
    }

    @Transactional
    public List<MainReceptionRequestDto> findByQueueId(long id) {

        log.info("Looking for reception requests with queue id: {}", id);

        List<ReceptionRequest> receptionRequests =
                requestDao.findByQueueId(id)
                        .orElseGet(ArrayList::new);

        return receptionRequests.stream()
                .map(r -> {
                    MainReceptionRequestDto mrd =
                            mapper.map(r, MainReceptionRequestDto.class);
                    return calculateReceptionEnd(mrd, r);
                })
                .collect(Collectors.toList());
    }

    @Transactional
    public MainReceptionRequestDto update(long id, ReceptionRequestDto newRequest) {
        log.info("Updating reception request with id: {}", id);
        ReceptionRequest oldRequest = find(id);
        if (!oldRequest.getReceptionDuration()
                .equals(newRequest.getReceptionDuration())) {

            log.info("Check if queue has enough free time");
            log.info("request duration: {}",
                    newRequest.getReceptionDuration().getDuration());

            MainReceptionQueueDto queue = newRequest.getQueue();
            if (queue.getFreeTime()
                    .compareTo(newRequest.getReceptionDuration().getDuration()) < 0) {
                log.error("Updating request for queue with no enough free time");
                log.error("queue id: {}, free time: {}; request duration: {}", queue.getFreeTime(),
                        queue.getFreeTime(), newRequest.getReceptionDuration().getDuration());
                throw new NoFreeTimeException("Sorry, but you can not change " +
                        "the problem(duration) of the request for " +
                        "queue for date: " + queue.getDate() +
                        ", for doctor with id = " + queue.getDoctor().getId());
            }
        }

        ReceptionRequest request = mapper.map(newRequest, ReceptionRequest.class);
        request.setId(id);

        //TODO remove anti-pattern
        MainReceptionRequestDto result =
                mapper.map(requestDao.update(request), MainReceptionRequestDto.class);

        result = calculateReceptionEnd(result, newRequest);
        return result;
    }

    @Transactional
    public void delete(long id) {
        log.info("Deleting reception request with id: {}", id);
        requestDao.remove(find(id));
    }

    private ReceptionRequest find(long id) {
        log.info("Find reception request with id: {}", id);
        try{
            ReceptionRequest result = requestDao.findById(id);
            if (result == null) {
                log.error("ReceptionRequest with id: {} was not found!", id);
                throw new NoSuchEntityException("There is no ReceptionRequest with id = " + id);
            } else {
                log.info("ReceptionRequest with id: {} was found!", id);
                return result;
            }
        } catch (NoResultException e) {
            log.error("There is no reception request with id: {}", id);
            throw new NoSuchEntityException("There is no reception request with id = " + id);
        }
    }


    private MainReceptionRequestDto calculateReceptionEnd(
            MainReceptionRequestDto mainRequestDto, ReceptionRequestDto requestDto) {
        log.info("Setting reception end instead of reception duration");
        mainRequestDto.setReceptionEnd(mainRequestDto.getReceptionStart()
                .plus(requestDto.getReceptionDuration().getDuration()));

        return mainRequestDto;
    }

    private MainReceptionRequestDto calculateReceptionEnd(
            MainReceptionRequestDto mainRequestDto, ReceptionRequest request) {
        log.info("Setting reception end instead of reception duration");
        mainRequestDto.setReceptionEnd(mainRequestDto.getReceptionStart()
                .plus(request.getReceptionDuration().getDuration()));

        return mainRequestDto;
    }
}
